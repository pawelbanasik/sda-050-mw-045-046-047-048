package pl.sdacademy.services01;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText timeEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        timeEdit = (EditText) findViewById(R.id.count_edit);

        findViewById(R.id.start_button).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int count = Integer.parseInt(timeEdit.getText().toString());

        Intent intent = new Intent(this, CounterService.class);
        intent.putExtra(CounterService.KEY_COUNT, count);
        startService(intent);
    }
}
