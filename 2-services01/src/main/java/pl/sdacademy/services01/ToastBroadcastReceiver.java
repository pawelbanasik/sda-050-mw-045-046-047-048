package pl.sdacademy.services01;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class ToastBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        int count = intent.getIntExtra(CounterService.KEY_COUNT, 0);
        Toast.makeText(context, "Toast: " + count, Toast.LENGTH_SHORT).show();
    }
}
