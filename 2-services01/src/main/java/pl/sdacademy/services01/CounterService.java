package pl.sdacademy.services01;

import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.util.Timer;
import java.util.TimerTask;

public class CounterService extends Service {

    public final static String ACTION_COUNT = "pl.sdacademy.services01.ACTION_COUNT";
    public final static String KEY_COUNT = "KEY_COUNT";

    private LocalBroadcastManager localBroadcastManager;
    private ToastBroadcastReceiver toastBroadcastReceiver;

    private int count;

    @Override
    public void onCreate() {
        IntentFilter intentFilter = new IntentFilter(ACTION_COUNT);
        toastBroadcastReceiver = new ToastBroadcastReceiver();

        localBroadcastManager = LocalBroadcastManager.getInstance(this);
        localBroadcastManager.registerReceiver(toastBroadcastReceiver, intentFilter);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        count = intent.getIntExtra(KEY_COUNT, 0);
        count();

        return START_NOT_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        localBroadcastManager.unregisterReceiver(toastBroadcastReceiver);
    }

    private void count() {
        new Timer().scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                count--;

                Log.d("TAG", "Odliczanie: " + count);

                Intent intent = new Intent(ACTION_COUNT);
                intent.putExtra(KEY_COUNT, count);
                localBroadcastManager.sendBroadcast(intent);

                if (count == 0) {
                    cancel();
                }
            }
        }, 0, 10000);
    }
}
