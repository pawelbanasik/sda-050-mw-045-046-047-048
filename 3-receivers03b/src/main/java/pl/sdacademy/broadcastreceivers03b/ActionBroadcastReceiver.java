package pl.sdacademy.broadcastreceivers03b;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class ActionBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent startActivityIntent = new Intent(context, MainActivity.class);
        context.startActivity(startActivityIntent);
    }
}
