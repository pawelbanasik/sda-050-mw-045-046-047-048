package pl.sdacademy.services03;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.util.Log;

public class FileUploadService extends IntentService {

    public static final String KEY_JOB_ID = "pl.sdacademy.services03.KEY_JOB_ID";
    public static final String KEY_SLEEP_TIME = "pl.sdacademy.services03.KEY_SLEEP_TIME";

    private static final String WORKER_THREAD_NAME = "FileUploadService";

    public FileUploadService() {
        super(WORKER_THREAD_NAME);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if (intent == null) {
            return;
        }

        int jobId = intent.getIntExtra(KEY_JOB_ID, 0);
        long sleepTime = intent.getLongExtra(KEY_SLEEP_TIME, 0) * 1000;

        Log.d("tag", "Starting job #" + jobId);

        try {
            Thread.sleep(sleepTime);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Log.d("tag", "Job #" + jobId + " finished!");
    }
}



