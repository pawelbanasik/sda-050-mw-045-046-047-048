package pl.sdacademy.services03;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private final static int DEFAULT_TIME = 10;

    private TextView countText;
    private EditText timeEdit;

    private int counter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        countText = (TextView) findViewById(R.id.counter_text);
        countText.setText(String.valueOf("0"));
        timeEdit = (EditText) findViewById(R.id.time_edit);
        timeEdit.setText(String.valueOf(DEFAULT_TIME));

        findViewById(R.id.start_button).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(this, FileUploadService.class);
        intent.putExtra(FileUploadService.KEY_JOB_ID, ++counter);
        intent.putExtra(FileUploadService.KEY_SLEEP_TIME, Long.parseLong(timeEdit.getText().toString()));
        startService(intent);

        countText.setText(String.valueOf(counter));
    }
}
