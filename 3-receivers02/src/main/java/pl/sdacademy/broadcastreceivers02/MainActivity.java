package pl.sdacademy.broadcastreceivers02;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    private IntentFilter networkFilter;
    private BroadcastReceiver networkReceiver;

    private View connectionErrorMessage;
    private View someButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        connectionErrorMessage = findViewById(R.id.connection_error_message);
        someButton = findViewById(R.id.some_button);

        networkFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        networkReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                boolean isConnected = isConnected();
                connectionErrorMessage.setVisibility(isConnected ? View.GONE : View.VISIBLE);
                someButton.setEnabled(isConnected);
            }
        };
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(networkReceiver, networkFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(networkReceiver);
    }

    public boolean isConnected() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        return networkInfo != null && networkInfo.isConnected()
                && (networkInfo.getType() == ConnectivityManager.TYPE_WIFI
                || networkInfo.getType() == ConnectivityManager.TYPE_MOBILE);
    }

}
