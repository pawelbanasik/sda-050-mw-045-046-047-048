package pl.sdacademy.broadcastreceivers01;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class BatteryReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        String message = getMessageForAction(intent.getAction(), context);
        showToast(message, context);
    }

    private String getMessageForAction(String action, Context context) {
        switch (action) {
            case Intent.ACTION_POWER_CONNECTED:
                return context.getString(R.string.power_connected);
            case Intent.ACTION_POWER_DISCONNECTED:
                return context.getString(R.string.power_disconnected);
            case Intent.ACTION_BATTERY_LOW:
                return context.getString(R.string.battery_low);
            default:
                throw new IllegalArgumentException("Unknown action");
        }
    }

    private void showToast(String message, Context context) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }
}
