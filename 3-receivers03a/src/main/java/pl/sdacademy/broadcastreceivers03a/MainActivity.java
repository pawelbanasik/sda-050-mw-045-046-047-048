package pl.sdacademy.broadcastreceivers03a;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    private static final String BROADCAST_ACTION = "pl.sdacademy.broadcastreceivers.BROADCAST_ACTION";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.send_broadcast_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                broadcastAction();
            }
        });
    }

    private void broadcastAction() {
        Intent intent = new Intent(BROADCAST_ACTION);
        sendBroadcast(intent);
    }
}
