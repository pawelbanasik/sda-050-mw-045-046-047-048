package pl.sdacademy.services02;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, TimerService.TimerServiceListener {

    private final static int DEFAULT_TIMER = 10;

    private TimerService timerService;

    private TextView timerText;
    private EditText timeEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        timerText = (TextView) findViewById(R.id.timer_text);
        timerText.setText(String.valueOf(DEFAULT_TIMER));
        timeEdit = (EditText) findViewById(R.id.time_input);
        timeEdit.setText(String.valueOf(DEFAULT_TIMER));

        findViewById(R.id.pause_button).setOnClickListener(this);
        findViewById(R.id.start_button).setOnClickListener(this);
        findViewById(R.id.stop_button).setOnClickListener(this);

        Intent intent = new Intent(this, TimerService.class);
        bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    public void onClick(View v) {

        int seconds = Integer.parseInt(timeEdit.getText().toString());

        switch (v.getId()) {
            case R.id.pause_button:
                timerService.pauseTimer(this);
                break;
            case R.id.start_button:
                timerService.startTimer(seconds, this);
                break;
            case R.id.stop_button:
                timerService.stopTimer(seconds, this);
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindService(serviceConnection);
    }

    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            timerService = ((TimerService.LocalBinder) service).getService();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            timerService = null;
        }
    };

    @Override
    public void onCounterUpdate(final int seconds) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                timerText.setText(String.valueOf(seconds));
            }
        });
    }
}
